# Relazione dell'esercitazione 3 (Stefano Volpe, #969766)

## Esercizio 1

### `WIFI-analisi1.cap`

Decido che, come elenco di password comuni userò `rockyou.txt`, che avevo
precedentemente estratto in `/usr/share/wordlists`.

Per individuare l'indirizzo MAC del punto di accesso senza fili alla rete,
esamino tramite Wireshark la registrazione del traffico di rete salvata in
`WIFI-analisi1.cap`. In particolare, trovo l'identificativo BSS nel pacchetto
1, frame Beacon: è `00:14:6c:7e:40:80`.

Proseguo ad attaccare la rete:

```bash
$ aircrack-ng -w /usr/share/wordlists/rockyou.txt -b 00:14:6c:7e:40:80 \
  WIFI-analisi1.cap
```

La password è `44445555`.

### `WIFI-analisi2.cap`

In questo caso, la strategia precedente di attacco non funziona, ma questo era
prevedibile: il processo di autentiazione che abbiamo registrato non è terminato
(si blocca dopo il pacchetto 2 di 4 a ogni tentativo).

## Esercizio 2

Analizzo il traffico con Wireshark. Filtro con
`http ip.host dreamliner.challs.cyberchallenge.it` e individuo le richieste
`POST`:

- il primo comando è `START`, che genera una nuova mappa;
- per curvare, si usano `TURNL` e `TURNR`;
- per andare dritto viene usato `DIVE`;
- per scendere di quota viene usato `BRAKE`.

Ora che so i comandi, posso giocare: il mio obiettivo è arrivare sulla cella
della pista di atterraggio e poi scendere sotto quota 0.

La _flag_ è:

```
CCIT{d4a4e6c3-bd3c-4112-9dad-5cb6d4e1ea21}
```

## Esercizio 3

Apro il file con Wireshark. Per filtrare e visionare solo i pacchetti sulla
porta 22, uso il filtro `tcp.port == 22``. Osservo che i pacchetti di mio
interesse sono erroneamente identificati come pacchetti SSH. Con il comando
"Decode as", faccio sì che vengano invece decodificati come pacchetti TCP,
poiché so che l'intruso sta usando questo protocollo. Infine, usando
"File"/"Export Objects"/"HTTP..." ho esportato le varie risorse per poterle
analizzare con calma.

Fra queste, un'immagine contiene la _flag_:

```
CCIT{webserver_on_22_WTF_3f25}
```

Tradotto: "Server web sulla porta 22. Cosa diamine...! 2f25", in riferimento
alla scelta del server web di usare una porta diversa dalla porta nota per HTTP.
