# Relazione dell'esercitazione 4 (Stefano Volpe, #969766)

Consulto il file delle _hash_:

```bash
cat Exercises.txt
```

## Esercizio 1

Individuo l'algoritmo usato per la prima _hash_:

```bash
hashid a4716c0b21b3e11ab40cbcecd2f2783804ae8ce2
```

Di tutte le possibili soluzioni restituite dal comando, parto da SHA-1, che è
molto diffusa. Mi serve la pagina di manuale di `hashcat` per capire quale
identificativo è assegnato dal comando a questo algoritmo:

```bash
hashcat --help
```

L'identificativo è 100. Tento un attacco con il dizionario `rockyou.txt` insieme
a un file di regole standard:

```bash
hashcat -a 0 -m 100 \
  -r /usr/share/hashcat/rules/InsidePro-PasswordsPro.rule \
  a4716c0b21b3e11ab40cbcecd2f2783804ae8ce2 \
  /usr/share/wordlists/rockyou.txt
```

Devo aspettare diversi minuti, ma alla fine ottengo `Portugal98!`. Il nome del
file contenente il testo cifrato, `file1.aes-256-ecb.txt`, mi fa capire che è
stato usato l'algoritmo di cifratura AES-256 in modalità ECB. Procedo alla
decifrazione:

```bash
openssl aes-256-ecb -d -in file1.aes-256-ecb.txt \
  -out file1.aes-256-ecb-plain.txt
```

Consulto il testo in chiaro così ottenuto:

```bash
cat file1.aes-256-ecb-plain.txt
```

Scopro che la prima bandiera è `kjf24jt0*s`. Mi viene detto che per il prossimo
esercizio dovrò usare entrambe la password.

## Esercizio 2

Individuo l'algoritmo usato per la seconda _hash_:

```bash
hashid 2bb80d537b1da3e38bd30361aa855686bde0eacd7162fef6a25fe97bf527a25b
```

Di tutte le possibili soluzioni restituite dal comando, parto da SHA-256, che è
molto diffusa. Mi serve la pagina di manuale di `hashcat` per capire quale
identificativo è assegnato dal comando a questo algoritmo:

```bash
hashcat --help
```

L'identificativo è 1400. Tento un attacco con il dizionario `rockyou.txt`
insieme a un file di regole standard:

```bash
hashcat -a 0 -m 1400 \
  -r /usr/share/hashcat/rules/InsidePro-PasswordsPro.rule \
  bc107137cda7aa074de2664a88247f2dfa54546923049ec929869edd6bc648a0 \
  /usr/share/wordlists/rockyou.txt
```

Ottengo `secret`. Il nome del file contenente il testo cifrato,
`file2.aes-256-cbc.txt`, mi fa capire che è stato usato l'algoritmo di cifratura
AES-256 in modalità CBC. Procedo alla decifrazione:

```bash
openssl aes-256-cbc -d -in file2.aes-256-cbc.txt \
  -out file2.aes-256-cbc-plain.txt
```

Consulto il testo così ottenuto:

```bash
cat file2.aes-256-cbc-plain.txt
```

Non è leggibile, quindi non può essere già il testo in chiaro desiderato. Avrò
bisogno anche della prossima _password_, quindi individuo l'algoritmo usato per
la terza _hash_:

```bash
hashid 6e1ec84e410f94b7b3999f1013a4e944
```

Di tutte le possibili soluzioni restituite dal comando, parto da MD5, che è
molto diffusa. Mi serve la pagina di manuale di `hashcat` per capire quale
identificativo è assegnato dal comando a questo algoritmo:

```hash
man hashcat
```

L'identificativo è 1400. Tento un attacco con il dizionario `rockyou.txt`
insieme a un file di regole standard:

```hash
hashcat -a 0 -m 0 \
  -r /usr/share/hashcat/rules/InsidePro-PasswordsPro.rule \
  6e1ec84e410f94b7b3999f1013a4e944 \
  /usr/share/wordlists/rockyou.txt
```

Ottengo `Andrea1.`. Partendo dal risultato parziale ottenuto dall'ultima
decriptazione, ne effettuo un'altra con quest'ultima _password_:

```bash
openssl aes-256-cbc -d -in file2.aes-256-cbc-plain.txt \
  -out file2.aes-256-cbc-plain2.txt
```

Consulto il file così ottenuto:

```bash
cat file2.aes-256-cbc-plain2.txt
```

Ottengo la seconda bandiera, `awo094fj^q`. Inoltre imparo che il sale è
`dd1b1n5`. Noto che è lo stesso sale usato in una delle esercitazioni
precedenti.

## Esercizio 3

Siccome la quarta _hash_ è già stata usata con il sale di cui sopra in una delle
esercitazioni precedenti, conosco già la _password_: è `qwerty`. Posso usarla
subito per la decriptazione. Non sapendo la modalità, provo entrambe quelle
viste. Ho successo quando uso la modalità ECB:

```bash
openssl aes-256-ecb -d -in file3.indovinalo.txt \
  -out file3.indovinalo-plain.txt
```

Consulto il testo in chiaro così ottenuto:

```bash
cat file3.indovinalo-plain.txt
```

Scopro che la terza bandiera è `ptr011zm7_w`.
